# Python
MAIN_NAME=dev.py
ENV_NAME=venv
PYTHON_VERSION=`which python3`

# Docker
DOCKER_IMAGE_NAME=bigj.icu
DOCKER_OPTIONS=--rm -it -p 5000:5000
DOCKER=docker
DOCKER_COMPOSE=docker-compose

.PHONY: run setup build rund killd clean cleand

all: rund

deploy: kill
	${DOCKER_COMPOSE} up -d

debug: kill
	${DOCKER_COMPOSE} up --build

build:
	${DOCKER_COMPOSE} build

kill:
	${DOCKER_COMPOSE} kill

setup:
	if [ -d ${ENV_NAME} ]; then \
		rm -rf ${ENV_NAME}; \
	fi
	if [ -a requirements.txt ]; then \
		touch requirements.txt; \
	fi
	which virtualenv && pip install virtualenv || true
	virtualenv -p ${PYTHON_VERSION} ${ENV_NAME}
	./${ENV_NAME}/bin/pip install -r requirements.txt

rung:
	if [ ! -d ${ENV_NAME} ]; then \
		make setup; \
	fi
	./${ENV_NAME}/bin/gunicorn -b 0.0.0.0:5000 -w 8 web:app #${MAIN_NAME}

run:
	if [ ! -d ${ENV_NAME} ]; then \
		make setup; \
	fi
	./${ENV_NAME}/bin/python ${MAIN_NAME}

clean: kill
	echo 'y' | ${DOCKER} system prune
	if [ -n "`${DOCKER} image list -q | grep ${DOCKER_IMAGE_NAME}`" ]; then \
		${DOCKER} rmi ${DOCKER_IMAGE_NAME}; \
	fi
	if [ -d ${ENV_NAME} ]; then \
		rm -rf ${ENV_NAME}; \
	fi
	if [ -n "`find . -name __pycache__`" ]; then \
		rm -rf `find . -name __pycache__`; \
	fi
