from os import environ
import multiprocessing

PORT=environ.pop('PORT') if 'PORT' in environ else '80'
WORKERS=environ.pop('WORKERS') if 'WORKERS' in environ else '2'
SITENAME=environ.pop('SITENAME') if 'SITENAME' in environ else 'DEFAULT'

bind='0.0.0.0:{}'.format(PORT)
workers=WORKERS
errorlog='web/data/error.log'
#capture_output=True
preload_app=True

# certfile='/etc/letsencrypt/live/{}/cert.pem'.format(SITENAME)
# keyfile='/etc/letsencrypt/live/{}/privkey.pem'.format(SITENAME)
# ca_certs='/etc/letsencrypt/live/{}/chain.pem'.format(SITENAME)
