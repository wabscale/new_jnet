FROM jmc1283/flasq-base

ENV CERTPATH=bigj.icu
ENV SITENAME=bigj.icu
ENV PORT=80
ENV WORKERS=2

COPY requirements.txt /flasq/requirements.txt
RUN pip3 install -r requirements.txt
RUN rm requirements.txt

RUN mkdir -p /files

COPY . /flasq


CMD gunicorn --config gunicorn_config.py --log-level debug web:app
