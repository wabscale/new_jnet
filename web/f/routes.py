from flask import send_from_directory, Blueprint, render_template, flash, request, url_for, redirect
from flask_login import current_user, login_required
from sqlalchemy.exc import IntegrityError
from werkzeug import secure_filename
import os

from .forms import UploadForm, UpdateForm
from ..app import app, db
from ..models import Files


files = Blueprint('files', __name__, url_prefix='/f')

@files.route('/', methods=['GET', 'POST'])
@login_required
def home():
    upload_form=UploadForm(action='upload')
    update_form=UpdateForm()
    try:
        if request.form.get('action') == 'upload' and upload_form.validate_on_submit():
            f=Files(
                filename=secure_filename(upload_form.file.data.filename),
                public=upload_form.public.data,
                user_id=current_user.id,
            )
            f.save_file(upload_form.file.data)
            db.session.add(f)
            flash('uploaded {filename}'.format(filename=f.filename))

        elif request.form.get('action') == 'update' and update_form.validate_on_submit():
            f=Files.query.get(int(update_form.id.data))

            if f.filename != update_form.filename.data:
                flash('rename successful') if f.rename(update_form.filename.data) else flash('error filename conflict')

            f.public=update_form.public.data

        elif request.form.get('action') == 'delete' and update_form.validate_on_submit():
            f=Files.query.get(int(update_form.id.data))
            f.delete_file()
            db.session.delete(f)
            flash('{filename} deleted'.format(filename=f.filename))

        db.session.commit()
    except IntegrityError as e:
        flash('error:{}'.format(e))
        db.session.rollback()

    update_forms=[UpdateForm() for _ in range(len(current_user.files))]
    for  i, file in enumerate(current_user.files):
        update_forms[i].populate(file)
    return render_template('files/home.html', update_forms=update_forms, upload_form=upload_form)

@files.route('/<path>', methods=['GET'])
def serve_file(path):
    try:
        file=Files.query.filter_by(filename=path).first()
        if not file.public and not current_user.is_authenticated:
            raise Exception()
        return send_from_directory(app.config['UPLOAD_PATH'], path)
    except:
        return 'not found', 404


