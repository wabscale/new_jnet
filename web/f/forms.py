from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms.fields import TextField, PasswordField, SubmitField, BooleanField, HiddenField
from wtforms.validators import InputRequired, Required, Optional


class UploadForm(FlaskForm):
    file=FileField('File', validators=[FileRequired()])
    action=HiddenField('upload')
    public=BooleanField()
    submit=SubmitField('Upload')

class UpdateForm(FlaskForm):
    id=HiddenField('id', [InputRequired()])
    filename=TextField('filename', [Optional()])
    public=BooleanField('public', [Optional()])

    def populate(self, file):
        self.id.data=file.id
        self.filename.data=file.filename
        self.public.data=file.public
