from werkzeug.security import generate_password_hash, check_password_hash
from os import remove as rm, rename as mv
from werkzeug import secure_filename
from flask_login import UserMixin
from .app import db, app


class Files(db.Model):
    id=db.Column(db.Integer, primary_key=True)
    user_id=db.Column(db.Integer, db.ForeignKey('users.id'))
    filename=db.Column(db.String(128), unique=True)
    public=db.Column(db.Boolean())

    owner=db.relationship('Users', backref=db.backref('files', lazy=True))

    def delete_file(self):
        try:
            rm(app.config['UPLOAD_PATH'] + self.filename)
        except FileNotFoundError:
            pass

    def rename(self, new_filename):
        new_filename=secure_filename(new_filename)
        if new_filename==self.filename:
            return True
        if Files.query.filter_by(filename=new_filename).first() is not None:
            return False
        mv(
            app.config['UPLOAD_PATH']+self.filename,
            app.config['UPLOAD_PATH']+new_filename,
        )
        self.filename=new_filename
        return True

    def save_file(self, data):
        data.save(app.config['UPLOAD_PATH'] + self.filename)

    def get_link(self):
        return 'https://bigj.icu/f/{filename}'.format(f.filename)

    @staticmethod
    def get(filename):
        """this will just get the file object associated with filename"""
        return File.query.filter_by(filename=filename).first()

class Users(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128), unique=True, index=True)
    password = db.Column(db.String(128), nullable=False)

    def get_id(self):
        return self.username

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def group_list(self):
        return ','.join(group.name for group in self.groups)

    def is_admin(self):
        return self.id == 0 and self.username == 'admin'

    @staticmethod
    def get_by(id):
        return Users.query.filter(id==id).first()

    @staticmethod
    def all():
        return Users.query.all()


# class Groups(db.Model):
#     id=db.Column(db.Integer, primary_key=True)
#     name=db.Column(db.String, primary_key=True)

#     members=db.relationship('Users', back_populates='users')
#     files=db.relationship('Files', back_populates='files')

#     @staticmethod
#     def all():
#         return Groups.query.all()
