from flask import send_from_directory, Blueprint, render_template, flash, request, url_for, redirect
from flask_login import current_user, login_required
from sqlalchemy.exc import IntegrityError
from werkzeug.security import generate_password_hash
import functools
import os

from ..app import app, db
from ..models import Users

#from .forms import NewUserForm, EditUserForm, NewGroupForm, EditGroupForm


manage = Blueprint('manage', __name__, url_prefix='/manage')

def admin_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.is_admin():
            return redirect(url_for('auth.login')), 403
        return view(**kwargs)
    return wrapped_view

@manage.route('/')
@login_required
@admin_required
def home():
    return render_template('manage/home.html')

# @manage.route('/newuser', methods=['GET','POST'])
# @login_required
# @admin_required
# def new_user():
#     form=NewUserForm()
#     if form.validate_on_submit():
#         try:
#             u=User(
#                 username=form.username.data
#             )
#             u.set_password(form.password.data)
#             db.session.add(u)
#             db.session.commit()
#             flash('created user {username}'.format(form.username.data))
#         except IntegrityError:
#             flash('error')
#             db.session.rollback()
#     return render_template('form.html', form=form)

# @manage.route('/edituser', methods=['GET','POST'])
# @login_required
# @admin_required
# def new_user():
#     form=EditUserForm()
#     form.user.choices = [
#         (user.id, user.username)
#         for user in Users.all()
#         if user.username != 'admin'
#     ]
#     if form.validate_on_submit():
#         try:
#             user=Users.get_by(form.user.data)
#             if form.delete.data:
#                 db.session.delete(user)
#             else: # new password
#                 user.set_paswword(form.password.data)
#             db.session.commit()
#             flash('success')
#         except IntegrityError:
#             flash('error')
#             db.session.rollback()
#     return render_template('form.html', form=form)

# @manage.route('/newgroup', methods=['GET','POST'])
# @login_required
# @admin_required
# def new_user():
#     form=NewGroupForm()
#     if form.validate_on_submit():
#         try:
#             group=Groups(
#                 name=form.name.data
#             )
#             db.session.add(group)
#             db.session.commit()
#         except IntegrityError:
#             flash('error')
#             db.session.rollback()
#     return render_template('form.html', form=form)

# @manage.route('/newgroup', methods=['GET','POST'])
# @login_required
# @admin_required
# def new_user():
#     form=EditGroupForm()
#     all_users=Users.all()

#     form.add_user.choices=[]
#     form.remove_user.choices=[]

#     if form.validate_on_submit():
#         try:
#             db.session.commit()
#         except IntegrityError:
#             flash('error')
#             db.session.rollback()
#     return render_template('form.html', form=form)
