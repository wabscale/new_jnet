from flask_wtf import FlaskForm
from wtforms.fields import TextField, SubmitField, HiddenField
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired, Required, Optional, Email

class EmailForm(FlaskForm):
    recipients=EmailField('recipient', validators=[Required(), Email()])
    message=TextField('message', validators=[Required()])
    send=SubmitField('Send')
