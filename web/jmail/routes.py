from flask import Blueprint, render_template, flash
from flask_login import login_required
from flask_mail import Message

from .forms import EmailForm
from ..app import db, mail

jmail=Blueprint('jmail', __name__, url_prefix='/jmail')

@jmail.route('/', methods=['GET','POST'])
@login_required
def home():
    email_form=EmailForm()
    if email_form.validate_on_submit():
        msg=Message(
            email_form.message.data,
            sender='bigj@bigj.icu',
            recipients=[email_form.recipients.data]
        )
        mail.send(msg)
        flash('email sent')
    return render_template('jmail/home.html', email_form=email_form)
