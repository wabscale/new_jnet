
import os
class Config():
    # SECRET_KEY='DEBUG'
    # UPLOAD_PATH='/root/projects/bigj.icu/upload_path/'
    SECRET_KEY=os.urandom(32)
    UPLOAD_PATH='/files/'

    # sqlalchemy
    SQLALCHEMY_DATABASE_URI = 'sqlite:///data/db.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # flask_mail
    # MAIL_SERVER='localhost'
    # MAIL_PORT=8025
    # MAIL_USE_TLS=True
    # MAIL_USE_SSL=True
